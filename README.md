# Krossover-challenge
Movie clip editing project

## Requirements

NodeJS 4.3.x or greater.
Ruby 2.0.x or greater.
Ruby Sass Gem 3.4.x or greater.

## Install

Install ruby sass gem.
```javascript
gem install sass
```

Clone this repo and Install packages with:
```javascript
npm install
```

There are two development environments: **develop** and **production**, to launch the dev environment:

```javascript
gulp watch
```

For production environment:

```javascript
gulp watch --production
```

Production Environment does additional steps like minification and uglyfication of code.

Then, navigate to:
```javascript
http://localhost:8080/
```


## Gulp Configuration

Almost all configuration is in build-config.json file at the root of the project, it consists mainly of output folder paths.

Both environments reuses the same gulp pipes. All pipes have conditional statements (gulp-if) to differentiate when they have to output the optimized version (they wait for the --production flag thanks to yargs plugin).

Index.html file uses *gulp-inject* to automatically inject javascript and css into it. Javascript files are parsed by browserify to resolve dependencies.

## Features

All basic features are implemented.

## Bonus Features

1. Hotkeys are implemented but with random buggy behaviour, I think it is due to videogular buggy behaviour when the video source changes.
2. Save to localstorage is implemented.
3. Tags are implemented, clips are not filtered by tag name.
4. Markers on the video are implemented by cue points.
5. Autoplay feature is implemented
6. Reuse of the videoplayer in other pages has partial support, the video player is abstracted into a directive and exposes an API via `api` attribute and has no other dependencies. The directive is in the same module as the rest of the application though since using several modules would require a different gulp and browserify setup.