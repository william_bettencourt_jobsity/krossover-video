module.exports = function ($q) {
    var self = this;
    this.videos = [{
        id: "",
        name: "",
        description: "",
        src: "",
        subvideos: {console: []}
    }];
    this.currentVideo = this.videos[0];

    this.removeFragment = function (fragment) {
        return $q(function (resolve, reject) {
            for (var i = self.currentVideo.subvideos.console.length; i--;) {
                if (self.currentVideo.subvideos.console[i].params.id == fragment.params.id) {
                    self.currentVideo.subvideos.console.splice(i, 1);
                    break;
                }
            }
            resolve({status: 200, extra: self.currentVideo});
        });
    };
    this.addFragment = function (fragment) {
        return $q(function (resolve, reject) {
            if (fragment.id !== "") {
                var fg = self.findFragment(fragment.id);
                if (fg) {
                    fg.params.name = fragment.name;
                    fg.params.description = fragment.description;
                    fg.params.tags = [];
                    fg.timeLapse.start = fragment.start;
                    fg.timeLapse.end = fragment.end;
                    resolve({status: 200});

                } else {
                    reject({status: 404, message: "Fragment Not Found"});
                }

            } else {
                var id = self.getUUID();
                while (self.findFragment(id) !== null) {
                    id = self.getUUID();
                }
                self.currentVideo.subvideos.console.push({
                    timeLapse: {
                        start: fragment.start,
                        end: fragment.end
                    },
                    params: {
                        id: id,
                        name: fragment.name,
                        tags: [],
                        description: fragment.description
                    },
                    onComplete: angular.noop
                });
                resolve({status: 200});
            }


        });
    };
    this.nextFragment = function (id) {
        var target = self.lastFragment();
        for (var i = self.currentVideo.subvideos.console.length; i--;) {
            if (self.currentVideo.subvideos.console[i].params.id == id) {
                if (i + 1 < self.currentVideo.subvideos.console.length) {
                    target = self.currentVideo.subvideos.console[i + 1];
                    break;
                }
            }
        }
        return target;

    };
    this.previousFragment = function (id) {
        var target = self.firstFragment();
        for (var i = self.currentVideo.subvideos.console.length; i--;) {
            if (self.currentVideo.subvideos.console[i].params.id == id) {
                if (i - 1 >= 0) {
                    target = self.currentVideo.subvideos.console[i - 1];
                    break;
                }
            }
        }
        return target;

    };
    this.firstFragment = function(){
        return (self.currentVideo.subvideos.console.length > 0) ? self.currentVideo.subvideos.console[0] : null;
    };
    this.lastFragment = function(){
        return (self.currentVideo.subvideos.console.length > 0) ? self.currentVideo.subvideos.console[self.currentVideo.subvideos.console.length - 1] : null;
    };
    this.findFragment = function (id) {
        var fragment = null;
        for (var i = self.currentVideo.subvideos.console.length; i--;) {
            if (self.currentVideo.subvideos.console[i].params.id == id) {
                fragment = self.currentVideo.subvideos.console[i];
                break;
            }
        }
        return fragment;
    };
    this.changeVideo = function (id) {
        return $q(function (resolve, reject) {
            var video = self.findVideo(id);
            if (video) {
                self.currentVideo = video;
                resolve({
                    status: 200,
                    extra: video
                });
            } else {
                reject({status: 404, message: "video not found"});
            }
        });
    };

    this.saveNewVideo = function (video) {
        return $q(function (resolve, reject) {
            if (video.id !== "") {
                var vd = self.findVideo(video.id);
                if (vd) {
                    vd.name = video.title;
                    vd.description = video.description;
                    vd.src = video.url;
                    vd.type = video.type;
                    resolve({status: 200, extra: vd});

                } else {

                    reject({status: 404, message: "Video Not Found"});
                }

            } else {
                var id = self.getUUID();
                // poor man's unique id: we iterate until we get an id that hasn't been used inserted before
                while (self.findVideo(id) !== null) {
                    id = self.getUUID();
                }
                var new_vid = {
                    id: id,
                    name: video.title,
                    src: video.url,
                    type: video.type,
                    description: video.description,
                    subvideos: {console: []}
                };
                self.videos.push(new_vid);
                resolve({status: 200, extra: new_vid});

            }
        });

    };
    this.getUUID = function () {
        // Loosely based on RFC4122 - uuid version 4 for Pseudo-Random Numbers, source: Stackoverflow
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
    this.saveStorage = function () {
        localStorage.setItem('krossover', JSON.stringify(self.videos));
    };
    this.loadStorage = function () {
        var bd = JSON.parse(localStorage.getItem('krossover'));
        if (!bd) {
            self.videos = [
                {
                    id: "1",
                    name: "Sintel Trailer",
                    src: "http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4",
                    type: "video/mp4",
                    description: "Blender Movie Trailer",
                    subvideos: {
                        console: [
                            {
                                timeLapse: {
                                    start: 0,
                                    end: 20
                                },
                                params: {
                                    id: "1",
                                    name: "Fragment 01",
                                    tags: [],
                                    description: "A little fragment 01"
                                },
                                onComplete: angular.noop
                            },
                            {
                                timeLapse: {
                                    start: 5,
                                    end: 10
                                },
                                params: {
                                    id: "2",
                                    name: "Fragment 02",
                                    tags: [],
                                    description: "A little fragment 02"
                                },
                                onComplete: angular.noop
                            },
                            {
                                timeLapse: {
                                    start: 30,
                                    end: 40
                                },
                                params: {
                                    id: "3",
                                    name: "Fragment 03",
                                    tags: [],
                                    description: "A little fragment 03"
                                },
                                onComplete: angular.noop
                            }
                        ]
                    }

                },
                {
                    id: "2",
                    name: "Big Buck Bunny",
                    src: "http://media.w3.org/2010/05/bunny/trailer.mp4",
                    type: "video/mp4",
                    description: "Another Blender Movie Trailer",
                    subvideos: {console: []}
                }
            ];
            this.currentVideo = this.videos[0];

        } else {
            self.videos = bd;
            for (var i = self.videos.length; i--;) {
                for (var x = self.videos[i].subvideos.console.length; x--;) {
                    self.videos[i].subvideos.console[x].timeLapse.start = parseInt(self.videos[i].subvideos.console[x].timeLapse.start);
                    self.videos[i].subvideos.console[x].timeLapse.end = parseInt(self.videos[i].subvideos.console[x].timeLapse.end);
                }
            }
            this.currentVideo = this.videos[0];
        }

    };

    this.findVideo = function (id) {
        var vid = null;
        for (var i = self.videos.length; i--;) {
            if (self.videos[i].id == id) {
                vid = self.videos[i];
                break;
            }
        }
        return vid;
    };
    this.loadStorage();

};