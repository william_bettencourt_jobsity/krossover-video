module.exports = function(app) {
    var video = require("./main/videoService");
    
    app.service("VideoService", ["$q", video]);
};