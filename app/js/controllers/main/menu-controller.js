module.exports = function ($scope, $uibModal, VideoService, $rootScope) {
    $scope.api = null;
    $scope.videos = VideoService.videos;
    $scope.selected = VideoService.currentVideo;

    $scope.selectFragment = function (key, fragment) {
        $scope.api.startFragment(fragment);
    };
    $scope.changeVideo = function (video) {
        var current = $scope.api.changeSource(video);
        $scope.selected = VideoService.currentVideo;

    };
    $scope.$on("keypress:106", function(e){
        $scope.api.playPreviousFragment();
    });
    $scope.$on("keypress:107", function(e){
        $scope.api.playNextFragment();
    });
    $scope.playVideo = function () {
        $scope.api.play();
    };
    $scope.saveStorage = function () {
        VideoService.saveStorage();
    };
    $scope.removeFragment = function (fragment) {
        $scope.api.removeFragment(fragment);
    };
    $scope.addDialog = function () {
        $uibModal.open({
            templateUrl: "partials/modals/new-video-modal.html",
            controller: "NewVideoController",
            resolve: {
                items: function () {
                    return {
                        id: "",
                        name: "",
                        description: "",
                        src: ""
                    };
                }
            }
        }).result.then(function (result) {
            VideoService.saveNewVideo(result);
        }, function () {
        });
    };
    $scope.editMainVideo = function () {
        $uibModal.open({
            templateUrl: "partials/modals/new-video-modal.html",
            controller: "NewVideoController",
            resolve: {
                items: function () {
                    return $scope.selected;
                }
            }
        }).result.then(function (result) {
            VideoService.saveNewVideo(result);
        }, function () {
        });
    };
    $scope.addFragment = function () {
        $uibModal.open({
            templateUrl: "partials/modals/new-fragment-modal.html",
            controller: "NewFragmentController",
            resolve: {
                items: function () {
                    return {
                        timeLapse: {
                            start: 0,
                            end: 0
                        },
                        params: {
                            id: "",
                            name: "",
                            description: ""
                        },
                        onComplete: angular.noop
                    };
                }
            }
        }).result.then(function (result) {
            $scope.api.addFragment(result);
        }, function () {
        });
    };
    $scope.editFragment = function (fragment) {
        $uibModal.open({
            templateUrl: "partials/modals/new-fragment-modal.html",
            controller: "NewFragmentController",
            resolve: {
                items: function () {
                    return fragment;
                }
            }
        }).result.then(function (result) {
            $scope.api.addFragment(result);
        }, function () {
        });
    };
};