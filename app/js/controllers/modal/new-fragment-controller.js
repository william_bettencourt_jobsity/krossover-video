/**
 * Created by William on 1/16/2016.
 */
module.exports = function ($scope, $uibModalInstance, VideoService, items) {
    $scope.video = {
        id: items.params.id,
        name: items.params.name,
        description: items.params.description,
        start: items.timeLapse.start,
        end: items.timeLapse.end
    };

    $scope.ok = function () {
        $uibModalInstance.dismiss();
    };
    $scope.saveFragment = function () {
        $uibModalInstance.close($scope.video);
    };
    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };


};