/**
 * Created by William on 1/16/2016.
 */
module.exports = function ($scope, $uibModalInstance, VideoService, items) {
    $scope.video = {
        id: items.id,
        title: items.name,
        description: items.description,
        type: items.type,
        url: items.src
    };

    $scope.ok = function () {
        $uibModalInstance.dismiss();
    };
    $scope.saveVideo = function () {
        $uibModalInstance.close($scope.video);
    };
    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };


};