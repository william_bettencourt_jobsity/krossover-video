module.exports = function(app) {
    var menuCtrl = require("./main/menu-controller"),
        newVideoCtrl = require("./modal/new-video-controller"),
        newFragmentCtrl = require("./modal/new-fragment-controller");
    
    app.controller("MenuController", ["$scope", "$uibModal", "VideoService", "$rootScope", menuCtrl]);
    app.controller("NewVideoController", ["$scope", "$uibModalInstance", "VideoService", "items", newVideoCtrl]);
    app.controller("NewFragmentController", ["$scope", "$uibModalInstance", "VideoService", "items", newFragmentCtrl]);

};