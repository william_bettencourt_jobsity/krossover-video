(function(){
    "use strict";
    var app = angular.module("KrossoverApp", [
        "ui.router",
        "ngAnimate",
        "ngMessages",
        "ui.bootstrap",
        "ngSanitize",
        "com.2fdevs.videogular",
        "com.2fdevs.videogular.plugins.controls",
        "com.2fdevs.videogular.plugins.overlayplay",
        "com.2fdevs.videogular.plugins.poster",
        "info.vietnamcode.nampnq.videogular.plugins.youtube",
        "angular-svg-round-progress"
    ]);
    
    
    //Load controllers, services and directives
    require("./routes")(app);
    require("./controllers/controllers")(app);
    require("./services/services")(app);
    require("./directives/directives")(app);
})();