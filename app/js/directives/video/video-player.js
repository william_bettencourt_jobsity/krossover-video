/**
 * Created by William on 1/17/2016.
 */
module.exports = function () {
    return {
        restrict: "E",
        templateUrl: "partials/directives/video/video-player.html",
        bindToController: {
            api: "=",
            ovEnabled: "=",
            current: "="
        },
        scope: {},
        controllerAs: "ctrl",
        controller: ["$scope", "$element", "VideoService", "$sce", function ($scope, $element, VideoService, $sce) {
            var self = this;

            self.last_fragment_played = null;


            self.internal_api = null;
            self.fragment = null;
            self.autoplay = false;

            self.config = {
                sources: [],
                theme: {
                    url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
                },
                cuePoints: []
            };
            self.onComplete = function (currentTime, timeLapse, params) {
                if (self.fragment && self.fragment.params.id == params.id) {
                    self.internal_api.stop();
                    self.autoplay = false;
                    if (self.ovEnabled) {
                        self.ovActivated = true;
                    } else {
                        self.fragmentComplete();
                    }
                }
            };
            self.onLeave = function (currentTime, timeLapse, params) {
                if (self.fragment && self.fragment.params.id == params.id) {
                    self.internal_api.stop();
                    self.autoplay = false;
                    if (self.ovEnabled) {
                        self.ovActivated = true;
                    } else {
                        self.fragmentComplete();
                    }
                }

            };
            self.fragmentComplete = function () {
                //self.autoplay = false;
                if (self.ovEnabled) {
                    if (self.last_fragment_played !== null) {
                        var next = VideoService.nextFragment(self.last_fragment_played);
                        if (next) self.api.startFragment(next);
                    }
                }
            };

            self.init = function () {
                self.config.sources = [{
                    src: $sce.trustAsResourceUrl(VideoService.currentVideo.src),
                    type: VideoService.currentVideo.type
                }];
                self.config.cuePoints = VideoService.currentVideo.subvideos;

                for (var i = 0; i < self.config.cuePoints.console.length; i++) {
                    self.config.cuePoints.console[i].onComplete = self.onComplete;
                    self.config.cuePoints.console[i].onLeave = self.onLeave;
                }
            };


            self.onPlayerReady = function (API) {
                self.internal_api = API;
            };


            self.overlayCompleted = function () {
                self.ovActivated = false;
                self.fragmentComplete();
            };


            self.api = {
                play: function () {
                    self.last_fragment_played = null;
                    self.internal_api.stop();
                    self.config.sources = [{src: $sce.trustAsResourceUrl(VideoService.currentVideo.src)}];

                    self.config.cuePoints = VideoService.currentVideo.subvideos;

                    for (var i = 0; i < self.config.cuePoints.console.length; i++) {
                        self.config.cuePoints.console[i].onComplete = $scope.onComplete;
                    }
                    self.internal_api.play();
                },
                stop: function () {
                    self.internal_api.stop();
                },
                removeFragment: function (fragment) {
                    self.last_fragment_played = null;
                    VideoService.removeFragment(fragment);
                },
                addFragment: function (fragment) {
                    self.last_fragment_played = null;
                    VideoService.addFragment(fragment).then(function (result) {
                            self.config.cuePoints = VideoService.currentVideo.subvideos;

                            for (var i = 0; i < self.config.cuePoints.console.length; i++) {
                                self.config.cuePoints.console[i].onComplete = $scope.onComplete;
                            }
                            return VideoService.currenVideo;

                        },
                        function (reason) {

                        });
                },
                playNextFragment: function(){
                    self.ovActivated = false;
                    if (self.last_fragment_played !== null) {
                        var next = VideoService.nextFragment(self.last_fragment_played);
                        if (next) self.api.startFragment(next);
                    } else {
                        var last = VideoService.lastFragment();
                        if (last) self.api.startFragment(last);
                    }
                },
                playPreviousFragment: function(){
                    if (self.last_fragment_played !== null) {
                        var prev = VideoService.previousFragment(self.last_fragment_played);
                        if (prev) self.api.startFragment(prev);
                    } else {
                        var first = VideoService.firstFragment();
                        if (first) self.api.startFragment(first);
                    }
                },
                changeSource: function (video) {
                    self.last_fragment_played = null;
                    self.autoplay = false;
                    self.internal_api.stop();
                    VideoService.changeVideo(video.id).then(function (result) {
                    });

                    self.config.sources = [{
                        src: $sce.trustAsResourceUrl(VideoService.currentVideo.src),
                        type: VideoService.currentVideo.type
                    }];


                    self.config.cuePoints = VideoService.currentVideo.subvideos;

                    for (var i = 0; i < self.config.cuePoints.console.length; i++) {
                        self.config.cuePoints.console[i].onComplete = $scope.onComplete;
                    }
                    self.internal_api.stop();
                    return VideoService.currenVideo;


                },
                startFragment: function (fragment) {
                    self.fragment = fragment;
                    self.autoplay = true;
                    self.last_fragment_played = fragment.params.id;
                    var new_src = VideoService.currentVideo.src + "#t=" + self.fragment.timeLapse.start + "," + self.fragment.timeLapse.end;
                    self.config.sources = [{
                        src: $sce.trustAsResourceUrl(new_src),
                        type: VideoService.currentVideo.type
                    }];
                    self.internal_api.play();
                    //self.config.cuePoints = {};
                }
            };
            self.init();
        }]
    };
};