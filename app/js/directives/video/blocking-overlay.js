/**
 * Created by William on 1/17/2016.
 */
module.exports = function () {
    return {
        restrict: "E",
        templateUrl: "partials/directives/video/blocking-overlay.html",
        scope: {
            enabled: "=",
            activated: "=",
            callback: "&"
        },
        transclude: true,
        controller: ["$scope", "$timeout", function ($scope, $timeout) {
            var self = this;
            self.seconds = 3;
            self.time = self.seconds * 1000;
            self.timestep = 1000;
            self.tick = 100 * self.timestep / self.time;

            $scope.$watch("activated", function () {
                if (self.timer) $timeout.cancel(self.timer);
                $scope.current = 0;
                $scope.seconds_left = self.seconds;
                if ($scope.enabled && $scope.activated) {

                    self.ticker();
                }

            });

            self.ticker = function(){
                if ($scope.current >= 100) {
                    $timeout.cancel(self.timer);
                    $scope.activated = false;
                    $scope.callback();

                }
                else {
                    $scope.current += self.tick;
                    $scope.seconds_left--;
                    self.timer = $timeout(self.ticker, self.timestep);
                }
            };


        }]
    };
};