module.exports = function(app) {
    var overlay = require("./video/blocking-overlay"),
        keypress = require("./keypress/keypress"),
        vplayer = require("./video/video-player");
    
    app.directive("blockingOverlay", [overlay]);
    app.directive("keypress", ["$document", "$rootScope", keypress]);
    app.directive("videoPlayer", [vplayer]);
};