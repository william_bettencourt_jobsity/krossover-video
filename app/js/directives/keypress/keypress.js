/**
 * Created by William on 1/17/2016.
 */
module.exports = function ($document, $rootScope) {
    return {
        restrict: "A",
        link: function postLink(scope, element, attributes){
            var self = this;
            $document.on("keypress", function(e) {
                $rootScope.$broadcast("keypress", e);
                $rootScope.$broadcast("keypress:" + e.which, e);
            });
        }
    };
};